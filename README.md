# gitlab.vim

This is a vim plugin (specifily for spacevim), for gitlab integration.

## Requirements

- The [glab cli tool](https://docs.gitlab.com/ee/integration/glab/)
- The [SpaceVim](https://spacevim.org/)

## How to use it

Authorise in glab using the auth command

```
glab auth login
```

Presonaly prefer an access token method. [Read this](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

### Merge requests
#### View opened MRs

```
:Glab mr list
```

It will open a window with a list. Press Enter on the specific one, to view a detailed information in the separate window. In this window you can:
- Press E in this window, to view a diff.
- Press Q to close this window and return to the MRs list.
- Press A to approve this MR.
- Press M to merge this MR.

#### Create an MR

```
:Glab mr new <TargetBranch> [<SourceBranch>]
```

It will open a window to edit a title, a description, to add a viewer and to assing this MR to somebody. Save it to create an MR, or press Q to quit.

### Issues
#### View issues

```
:Glab issue list [flags]
```

It will open a window with a list of all issues. Select one to see detailed info. If you want to comment it, press n. It will open a separate buffer to write a comment. For more commands see *:help gitlab*.

#### Creating issues
```
:Glab issue new [flags]
```

### CI
#### View pipelines
```
:Glab ci list [flags]
```
Select a pipeline in the list to see detailed info.

If you want to run a pipeline on selected branch:
```
:Glab ci run [flags]
```

For more commands see *:help gitlab*

## More info
See [glab cli tool](https://docs.gitlab.com/ee/integration/glab/) documentation for more commands. You can try and run not metioned here commands too! Just be sure that they don't require some input :)

