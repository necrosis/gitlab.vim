"=============================================================================
" gitlib.vim --- gitlab plugin for spacevim
" Copyright (c) 
" Author: Necrosis Aumer < necrosis_t@mail.ru >
" URL: https://gitlab.com/necrosis/gitlab.vim 
" License: GPLv3
"=============================================================================

if exists('g:GitLab_api_plugin_loaded')
    finish
endif

let g:glab_exe = get(g:, 'glab_exe', 'glab')

command! -nargs=+ -complete=custom,gitlab#complete Glab call gitlab#run(<f-args>)

call SpaceVim#plugins#projectmanager#reg_callback(function('gitlab#utils#startup_setup'))

let g:GitLab_api_plugin_loaded = 1
