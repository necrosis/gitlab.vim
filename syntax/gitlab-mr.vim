if exists('b:current_syntax')
    finish
endif
let b:current_syntax = 'gitlab-mr'

syn include @GitlabDiff syntax/diff.vim

syntax case ignore
syn match GitlabMrInfo '^\w\+:'
syn match GitlabMrLoading '^\.\.\.loading'
syn match GitlabMrRemoveLine '^\-.*'
syn match GitlabMrAddLine /^+.*/
syn match GitlabMrModifyLine '^@@.*'

hi def link GitlabMrInfo Constant
hi def link GitlabMrLoading Constant
hi def link GitlabMrRemoveLine Constant
hi def link GitlabMrAddLine Include
hi def link GitlabMrModifyLine Statement
