if exists('b:current_syntax')
    finish
endif
let b:current_syntax = 'gitlab-issue-comment'
syntax case ignore global
syn match GitlabIssueComment '^\#.*'

hi def link GitlabIssueComment Comment
