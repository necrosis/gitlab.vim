if exists('b:current_syntax')
    finish
endif
let b:current_syntax = 'gitlab-mr-create'

syntax case ignore
syn match GitlabNewComment '^\s*#.*'
syn match GitlabNewAssign '^\s*assign:'
syn match GitlabNewReviewer '^\s*reviewers:'

hi def link GitlabNewComment Comment
hi def link GitlabNewAssign Constant
hi def link GitlabNewReviewer Constant
