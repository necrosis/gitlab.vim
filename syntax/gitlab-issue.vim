if exists('b:current_syntax')
    finish
endif
let b:current_syntax = 'gitlab-issue'

syn include @GitlabDiff syntax/diff.vim

syntax case ignore
syn match GitlabIssueTitle '^title:\s*.*'
syn match GitlabIssueState '^state:'
syn match GitlabIssueAuthor '^author:'
syn match GitlabIssueLables '^labels:'
syn match GitlabIssueComments '^comments:'
syn match GitlabIssueAssignis '^assignees:'
syn match GitlabIssueCommentsNotes '^comments/notes:'
syn match GitlabCommentHeader '.\+\scommented\s\d\+-\d\+-\d\+\s*\d\+:\d\+:\d\+.\+'
syn region GitlabIssueCodeBlock start='^\`\`\`' end='^\`\`\`' contains=ALL keepend

hi def link GitlabIssueTitle Type
hi def link GitlabIssueState Constant
hi def link GitlabIssueAuthor Constant
hi def link GitlabIssueLables Constant
hi def link GitlabIssueComments Constant
hi def link GitlabIssueAssignis Constant
hi def link GitlabIssueCommentsNotes Type
hi def link GitlabCommentHeader Constant
hi def link GitlabIssueCodeBlock Comment
