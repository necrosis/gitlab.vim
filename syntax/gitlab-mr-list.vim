if exists('b:current_syntax')
    finish
endif
let b:current_syntax = 'gitlab-mr-list'
syntax case ignore global
syn match GitlabMrNum '^\!\d\+'
syn match GitlabBranch '(.\{-})'

hi def link GitlabMrNum Conditional
hi def link GitlabBranch Conditional
