if exists('b:current_syntax')
    finish
endif
let b:current_syntax = 'gitlab-issue-list'
syntax case ignore global
syn match GitlabIssueNum '^\#\d\+'
syn match GitlabIssueProject '.+\/.+\#\d+'

hi def link GitlabIssueNum Conditional
hi def link GitlabIssueProject Include
