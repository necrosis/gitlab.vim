if exists('b:current_syntax')
    finish
endif
let b:current_syntax = 'gitlab-job-trace'
syntax case ignore global
syn match GitlabJobPreparing '^Preparing.\+'
syn match GitlabJobGetting '^Getting.\+'
syn match GitlabJobExecuting '^Executing.\+'
syn match GitlabJobCleaning '^Cleaning.\+'
syn match GitlabJobFetching '^Fetching.\+'
syn match GitlabJobCreating '^Created.\+'
syn match GitlabJobCheking '^Cheking.\+'
syn match GitlabJobSkipping '^Skipping.\+'
syn match GitlabJobCommand '^\$.\+'
syn match GitlabJobError '^ERROR:.\+'

hi def link GitlabJobPreparing Type
hi def link GitlabJobGetting Type
hi def link GitlabJobExecuting Type
hi def link GitlabJobCleaning Type
hi def link GitlabJobFetching Include
hi def link GitlabJobCreating Include
hi def link GitlabJobCheking Include
hi def link GitlabJobSkipping Include
hi def link GitlabJobCommand Include
hi def link GitlabJobError Constant
