if exists('b:current_syntax')
    finish
endif
let b:current_syntax = 'gitlab-pipelines-list'
syntax case ignore global
syn match GitlabPipelineStatusRunning '^(running)'
syn match GitlabPipelineStatusPending '^(pending)'
syn match GitlabPipelineStatusSuccess '^(success)'
syn match GitlabPipelineStatusFailed '^(failed)'
syn match GitlabPipelineStatusCanceled '^(canceled)'
syn match GitlabPipelineStatusSkipped '^(skipped)'
syn match GitlabJobNumber '\#\d\+'

hi def link GitlabJobNumber Conditional
hi def link GitlabPipelineStatusRunning Statement
hi def link GitlabPipelineStatusPending Statement
hi def link GitlabPipelineStatusSuccess Include
hi def link GitlabPipelineStatusFailed Constant
hi def link GitlabPipelineStatusCanceled Constant
hi def link GitlabPipelineStatusSkipped Comment
