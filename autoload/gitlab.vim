"=============================================================================
" gitlib.vim --- gitlab plugin for spacevim
" Copyright (c) 
" Author: Necrosis Aumer < necrosis_t@mail.ru >
" URL: https://gitlab.com/necrosis/gitlab.vi
" License: GPLv3
"=============================================================================


function! gitlab#run(...) abort
    let block = get(a:000, 0, '')
    let cmd = get(a:000, 1, '')
    if block ==# 'mr'
        if cmd ==# 'list'
            call gitlab#mr#show_mr_list(a:000[2:])
        elseif cmd ==# 'new'
            call call(function("gitlab#mr#new"), a:000[2:])
        endif
    elseif block ==# 'issue'
        if cmd ==# 'list'
            call gitlab#issue#show_list(a:000[2:])
        elseif cmd ==# 'new'
            call gitlab#issue#new(a:000[2:])
        endif
    elseif block ==# 'ci'
        if cmd ==# 'list'
            call gitlab#ci#show_list(a:000[2:])
        elseif cmd ==# 'run'
            call gitlab#ci#run(a:000[2:])
        elseif cmd ==# 'lint'
            call gitlab#ci#lint(a:000[2:])
        elseif cmd ==# 'trace'
            let flags = a:000[2:]
            call gitlab#ci#trace(flags)
        endif
    else
        call gitlab#utils#run(a:000)
    endif
endfunction


function! gitlab#complete(ArgLead, CmdLine, CursorPos) abort
    let str = a:CmdLine[:a:CursorPos-1]
    if str =~# '^Glab\s\+[a-zA-Z]*$'
        return join(['mr', 'issue', 'release', 'user', 'ci'], "\n")
    elseif str =~# '^Glab\s\+mr\s*$'
        return join(['list', 'new'], "\n")
    elseif str =~# '^Glab\s\+mr\s\+new\s\+.*$'
        return join(g:gitlab_existing_branches , "\n")
    elseif str =~# '^Glab\s\+issue\s*$'
        return join(['list', 'new'], "\n")
    elseif str =~# '^Glab\s\+ci\s*$'
        return join(['list', 'run', 'trace', 'lint'], "\n")
    elseif str =~# '^Glab\s\+ci\s\+lint\s\+.*$'
        return "%\n" . join(getcompletion(a:ArgLead, 'file'), "\n")
    else
        return ''
    endif
endfunction
