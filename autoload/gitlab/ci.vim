"=============================================================================
" gitlib.vim --- gitlab plugin for spacevim
" Copyright (c) 
" Author: Necrosis Aumer < necrosis_t@mail.ru >
" URL: https://gitlab.com/necrosis/gitlab.vim
" License: GPLv3
"=============================================================================


let s:JOB = SpaceVim#api#import('job')
let s:BUFFER = SpaceVim#api#import('vim#buffer')
let s:NOTI = SpaceVim#api#import('notify')

let s:bufnr = -1
let s:pipeline_trace_buffer = -1
let s:cwd = ''

function! gitlab#ci#show_list(flags) abort
  call s:show_pipelines_list(a:flags)
endfunction

function! gitlab#ci#run(flags) abort
  call s:run_pipeline(a:flags)
endfunction

function! gitlab#ci#lint(flags) abort
  call s:lint_yaml(a:flags)
endfunction

function! gitlab#ci#trace(flags) abort
  call s:trace_pipeline(a:flags)
endfunction

" -------------------------- Pipelines List
function! s:open_pipelines_list_buffer() abort
  let bp = bufnr('%')
  edit gitlab://pipelines_list
  normal! "_dd
  setl nobuflisted
  setl nomodifiable
  setl nonumber norelativenumber
  setl buftype=nofile
  setl bufhidden=wipe
  setf gitlab-pipelines-list
  nnoremap <buffer><silent> q :call <SID>close_win()<CR>
  return bufnr('%')
endfunction

function! s:show_pipelines_list(flags) abort
  let cmd = ['glab', 'ci', 'list'] + a:flags
  let s:cwd = expand('%:p:h')
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_pipelines_list_stderr'),
          \ 'on_stdout' : function('s:on_pipelines_list_stdout'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_pipelines_list_stderr(id, data, event) abort
  for data in a:data
    call s:NOTI.notify(data, 'WarningMsg')
  endfor
endfunction

function! s:on_pipelines_list_stdout(id, data, event) abort
  if !bufexists(s:bufnr)
    let s:bufnr = s:open_pipelines_list_buffer()
  endif
  call s:BUFFER.buf_set_lines(s:bufnr, getline('$') ==# '' ? 0 : -1 , -1, 0, a:data)
endfunction

function! s:close_win() abort
  try
    bp
  catch /^Vim\%((\a\+)\)\=:E85/
    bd
  endtry
endfunction

" -------------------------- Pipeline Run
function! s:run_pipeline(flags) abort
  let cmd = ['glab', 'ci', 'run'] + a:flags
  let s:cwd = expand('%:p:h')
  let s:show_lines = []
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_pipeline_run_stderr'),
          \ 'on_exit' : function('s:on_pipeline_run_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_pipeline_run_stderr(id, data, event) abort
  for data in a:data
    call s:NOTI.notify(data, 'WarningMsg')
  endfor
endfunction

function! s:on_pipeline_run_exit(id, data, event) abort
  if len(s:show_lines) > 0
    call s:NOTI.notify(s:show_lines[-1], 'WarningMsg')
  else
    call s:NOTI.notify('Pipeline started')
  endif
endfunction

" -------------------------- YAML Lint
function! s:lint_yaml(flags) abort
  let cmd = ['glab', 'ci', 'lint'] + a:flags
  let s:cwd = expand('%:p:h')
  let s:show_lines = []
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_pipeline_list_stderr'),
          \ 'on_stdout' : function('s:on_pipeline_list_stdout'),
          \ 'on_exit' : function('s:on_pipeline_list_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_pipeline_list_stderr(id, data, event) abort
  for data in a:data
    call s:NOTI.notify(data, 'WarningMsg')
  endfor
endfunction

function! s:on_pipeline_list_stdout(id, data, event) abort
  let s:show_lines += a:data
endfunction

function! s:on_pipeline_list_exit(id, data, event) abort
  call s:NOTI.notify(s:show_lines[-1])
endfunction

" -------------------------- Pipeline Trace
function! s:open_pipelines_trace_buffer() abort
  let bp = bufnr('%')
  edit gitlab://pipeline_trace
  normal! "_dd
  setl nobuflisted
  setl nomodifiable
  setl nonumber norelativenumber
  setl buftype=nofile
  setl bufhidden=wipe
  setf gitlab-job-trace-list
  nnoremap <buffer><silent> <Cr> :call <SID>show_job_trace()<CR>
  nnoremap <buffer><silent> q :call <SID>close_win()<CR>
  return bufnr('%')
endfunction

function! s:trace_pipeline(flags) abort
  let s:cwd = expand('%:p:h')

  if !bufexists(s:pipeline_trace_buffer)
    let s:pipeline_trace_buffer = s:open_pipelines_trace_buffer()
  endif

  let cmd = ['glab', 'ci', 'trace'] + a:flags
  let s:show_lines = []
  let job_id = s:JOB.start(cmd,
        \ {
          \ 'on_stdout' : function('s:on_pipeline_trace_stdout'),
          \ 'on_exit' : function('s:on_pipeline_trace_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )

  sleep 5
  call s:JOB.stop(job_id)
endfunction

function! s:on_pipeline_trace_stdout(id, data, event) abort
  let s:show_lines += a:data
endfunction

function! s:on_pipeline_trace_exit(id, data, event) abort
  if len(s:show_lines) == 0
    call s:NOTI.notify('Jobs not found')
    return
  endif

  let jobs_list = []
  for line in s:show_lines
    if matchstr(line, '(\d\+)') != ''
        call add(jobs_list, line[1:])
    endif
  endfor

  call s:BUFFER.buf_set_lines(s:pipeline_trace_buffer , 0, -1, 0, jobs_list)
endfunction

function! s:open_show_trace_job_buffer() abort
  rightbelow vsplit gitlab://trace_job
  normal! "_dd
  setl nobuflisted
  setl nomodifiable
  setl nonumber norelativenumber
  setl buftype=nofile
  setl bufhidden=wipe
  setf gitlab-job-trace
  nnoremap <buffer><silent> q :q<CR>
  return bufnr('%')
endfunction

let s:show_trace_job_buffer = -1
function! s:show_job_trace() abort
  echo getline('.')
  let trace_job_id = matchstr(getline('.'), '(\d\+)')[1:-2]

  if !bufexists(s:show_trace_job_buffer)
    let s:show_trace_job_buffer = s:open_show_trace_job_buffer()
  endif

  let s:show_lines = []
  let cmd = ['glab', 'ci', 'trace', trace_job_id]
  call s:JOB.start(cmd,
        \ {
          \ 'on_stdout' : function('s:on_show_trace_job_stdout'),
          \ 'on_stderr' : function('s:on_show_trace_job_stderr'),
          \ 'on_exit' : function('s:on_show_trace_job_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_show_trace_job_stdout(id, data, event) abort
  for data in a:data
    let cleaned_from_ascii = substitute(data, '.\+', '', 'ge')
    let cleaned_from_ascii = substitute(cleaned_from_ascii, '\e\[[0-9K;]*m*', '', 'gc')
    call add(s:show_lines, cleaned_from_ascii)
  endfor
endfunction

function! s:on_show_trace_job_stderr(id, data, event) abort
  for data in a:data
    call s:NOTI.notify(data, 'WarningMsg')
  endfor
endfunction

function! s:on_show_trace_job_exit(id, data, event) abort
  call s:BUFFER.buf_set_lines(s:show_trace_job_buffer, 0 , -1, 0, s:show_lines)
  let g:_some_show_lines = s:show_lines
endfunction
