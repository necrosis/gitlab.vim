"=============================================================================
" gitlib.vim --- gitlab plugin for spacevim
" Copyright (c) 
" Author: Necrosis Aumer < necrosis_t@mail.ru >
" URL: https://gitlab.com/necrosis/gitlab.vim
" License: GPLv3
"=============================================================================


let s:JOB = SpaceVim#api#import('job')
let s:BUFFER = SpaceVim#api#import('vim#buffer')
let s:NOTI = SpaceVim#api#import('notify')

let s:bufnr = -1
let s:cwd = ''
let s:show_issue_buffer = -1
let s:issue_number = -1

function! gitlab#issue#show_list(flags) abort
  call s:show_issue_list(a:flags)
endfunction

function! gitlab#issue#new(flags) abort
  call s:create_new_issue_buffer(a:flags)
endfunction

" -------------------------- Issue List
function! s:open_issue_buffer() abort
  let bp = bufnr('%')
  edit gitlab://issue_list
  normal! "_dd
  setl nobuflisted
  setl nomodifiable
  setl nonumber norelativenumber
  setl buftype=nofile
  setl bufhidden=wipe
  setf gitlab-issue-list
  nnoremap <buffer><silent> <Cr> :call <SID>show_issue()<CR>
  nnoremap <buffer><silent> q :call <SID>close_win()<CR>
  return bufnr('%')
endfunction

function! s:on_issue_list_stdout(id, data, event) abort
  if !bufexists(s:bufnr)
    let s:bufnr = s:open_issue_buffer()
  endif
  call s:BUFFER.buf_set_lines(s:bufnr, getline('$') ==# '' ? 0 : -1 , -1, 0, a:data)
endfunction

function! s:on_issue_list_stderr(id, data, event) abort
  for data in a:data
    call s:NOTI.notify(data, 'WarningMsg')
  endfor
endfunction

function! s:show_issue_list(flags) abort
  let cmd = ['glab', 'issue', 'list'] + a:flags
  let s:cwd = expand('%:p:h')
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_issue_list_stderr'),
          \ 'on_stdout' : function('s:on_issue_list_stdout'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:close_issue_window() abort
  if bufexists(s:show_issue_buffer)
    exe 'bd ' . s:show_issue_buffer
  endif
endfunction

function! s:close_win() abort
  call s:close_issue_window()
  try
    bp
  catch /^Vim\%((\a\+)\)\=:E85/
    bd
  endtry
endfunction

" -------------------------- Issue View
function! s:show_issue() abort
  let s:issue_number = matchstr(getline('.'), '^\#\(\d\+\)')
  if s:issue_number == -1
    return
  endif
  if !bufexists(s:show_issue_buffer)
    let s:show_issue_buffer = s:open_show_issue_buffer()
  endif
  let cmd = ['glab', 'issue', 'show', '--comments', s:issue_number[1:len(s:issue_number)]]
  let s:show_lines = []
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_show_issue_stderr'),
          \ 'on_stdout' : function('s:on_show_issue_stdout'),
          \ 'on_exit' : function('s:on_show_issue_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_show_issue_stdout(id, data, event) abort
  let s:show_lines += a:data
endfunction

function! s:on_show_issue_stderr(id, data, event) abort
  for data in a:data
    call s:NOTI.notify(data, 'WarningMsg')
  endfor
endfunction

function! s:on_show_issue_exit(id, data, event) abort
  call s:BUFFER.buf_set_lines(s:show_issue_buffer, 0 , -1, 0, s:show_lines)
endfunction

function! s:open_show_issue_buffer() abort
  rightbelow vsplit gitlab://show_issue
  normal! "_dd
  setl nobuflisted
  setl nomodifiable
  setl nonumber norelativenumber
  setl buftype=nofile
  setl bufhidden=wipe
  setf gitlab-issue
  nnoremap <buffer><silent> q :q<CR>
  nnoremap <buffer><silent> n :call <SID>create_new_comment_buffer()<CR>
  nnoremap <buffer><silent> c :call <SID>close_issue()<CR>
  nnoremap <buffer><silent> d :call <SID>delete_issue()<CR>
  nnoremap <buffer><silent> W :call <SID>show_issue_on_web()<CR>
  return bufnr('%')
endfunction

function! s:show_issue_on_web() abort
  let cmd = ['glab', 'issue', 'view', s:issue_number[1:len(s:issue_number)], '--web']
  call s:JOB.start(cmd,
        \ {
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

" -------------------------- Issue Close
function! s:close_issue() abort
  let cmd = ['glab', 'issue', 'close', s:issue_number[1:len(s:issue_number)]]
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_close_issue_error'),
          \ 'on_exit' : function('s:on_close_issue_finished'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

let s:close_issue_message = ''
function! s:on_close_issue_error(id, data, event) abort
  for data in a:data
    let s:close_issue_message += data
  endfor
endfunction

function! s:on_close_issue_finished(id, data, event) abort
  if strlen(s:close_issue_message) > 0
    call s:NOTI.notify(s:close_issue_message)
  else
    call s:NOTI.notify('Issue closed')
  endif

  call s:close_this_buffer(s:show_issue_buffer)
endfunction

" -------------------------- Issue Delete
function! s:delete_issue() abort
  let cmd = ['glab', 'issue', 'delete', s:issue_number[1:len(s:issue_number)]]
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_delete_issue_error'),
          \ 'on_exit' : function('s:on_delete_issue_finished'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

let s:delete_issue_message = ''
function! s:on_delete_issue_error(id, data, event) abort
  for data in a:data
    let s:delete_issue_message += data
  endfor
endfunction

function! s:on_delete_issue_finished(id, data, event) abort
  if strlen(s:delete_issue_message) > 0
    call s:NOTI.notify(s:delete_issue_message)
  else
    call s:NOTI.notify('Issue deleted')
  endif

  call s:close_this_buffer(s:show_issue_buffer)
endfunction

" -------------------------- Issue Comment
let s:new_comment_buffer = -1
function! s:create_new_comment_buffer() abort
  if !bufexists(s:new_comment_buffer)
    let s:new_comment_buffer = s:open_new_comment_buffer()
  endif

  call s:BUFFER.buf_set_lines(s:new_comment_buffer, 0, -1, 0, ["# Write comment below"])
endfunction

function! s:open_new_comment_buffer() abort
  rightbelow split gitlab://issue_comment
  normal! "_dd
  setl nobuflisted
  setl modifiable
  setl nonumber norelativenumber
  exe 'file' fnameescape('_new_issue_comment_temp_file_')
  setl buftype=acwrite
  setl bufhidden=wipe
  setf gitlab-comment
  nnoremap <buffer><silent> q :call <SID>close_new_comment_win()<CR>
  augroup NewIssueCommentCreation
    autocmd!
    autocmd BufWriteCmd _new_issue_comment_temp_file_ call <SID>push_comment()
  augroup END
  return bufnr('%')
endfunction

function! s:push_comment() abort
  let durty_comment = s:BUFFER.buf_get_lines(s:new_comment_buffer, 1, -1, 0)

  let durty_comment_split = split(join(durty_comment, '\n'), '"')
  let covered_comment = join(durty_comment_split, '\\"')

  let cmd = [
        \ 'glab', 'issue', 'note',
        \ '--message', covered_comment,
        \ s:issue_number[1:len(s:issue_number)]
        \]

  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_comment_issue_error'),
          \ 'on_exit' : function('s:on_comment_issue_finished'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

let s:error_message_on_creation = ''
function! s:on_comment_issue_error(id, data, event) abort
  for data in a:data
    let s:error_message_on_creation += data
  endfor
endfunction

function! s:on_comment_issue_finished(id, data, event) abort
  if strlen(s:error_message_on_creation) > 0
    call s:NOTI.notify(s:error_message_on_creation, 'WarningMsg')
  else
    call s:NOTI.notify('Comment created')
  endif
  call s:close_new_comment_win()
endfunction

function! s:close_new_comment_win() abort
  call s:close_this_buffer(s:new_comment_buffer)
endfunction

function! s:close_this_buffer(buffer) abort
  if bufexists(a:buffer)
    exe 'bd! ' . a:buffer
  endif

  try
    bp
  catch /^Vim\%((\a\+)\)\=:E85/
    bd
  endtry
endfunction

" -------------------------- Issue Create
let s:new_issue_buffer = -1
let s:new_issue_flags = []
function! s:create_new_issue_buffer(flags) abort
  let s:new_issue_flags = a:flags
  let s:cwd = expand('%:p:h')

  if !bufexists(s:new_issue_buffer)
    let s:new_issue_buffer = s:open_new_issue_buffer()
  endif

  call s:BUFFER.buf_set_lines(s:new_issue_buffer, 0, -1, 0, 
        \ [
        \ "# Write a title below", 
        \ "", 
        \ "# Write description below",
        \ ""
        \])
endfunction

function! s:open_new_issue_buffer() abort
  rightbelow vsplit gitlab://issue_buffer
  normal! "_dd
  setl nobuflisted
  setl modifiable
  setl nonumber norelativenumber
  exe 'file' fnameescape('_new_issue_temp_file_')
  setl buftype=acwrite
  setl bufhidden=wipe
  setf gitlab-issue-create
  nnoremap <buffer><silent> q :call <SID>close_new_issue_win()<CR>
  augroup NewIssueCreation
    autocmd!
    autocmd BufWriteCmd _new_issue_temp_file_ call <SID>create_issue()
  augroup END
  return bufnr('%')
endfunction

function! s:create_issue() abort
  let durty_creation_info = s:BUFFER.buf_get_lines(s:new_issue_buffer, 0, -1, 0)
  let creation_info = []
  for line in durty_creation_info
    if matchstr(line, '^\s*\#') ==# ''
      call add(creation_info, line)
    endif
  endfor

  let title = get(creation_info, 0, '')
  call remove(creation_info, 0)

  let uncleared_description = join(creation_info, '\n')
  let parted_description = split(uncleared_description, '"')
  let covered_description = join(parted_description, '\\"')

  let cmd = [
        \ 'glab', 'issue', 'new', 
        \ '-t', title, 
        \ '--description', covered_description, 
        \ '--no-editor' 
        \] + s:new_issue_flags

  setl nomodified

  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_create_issue_stderr'),
          \ 'on_exit' : function('s:on_create_issue_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

let s:error_message_on_issue_creation = ''
function! s:on_create_issue_stderr(id, data, event) abort
  for data in a:data
    let s:error_message_on_issue_creation += data
  endfor
endfunction

function! s:on_create_issue_exit(id, data, event) abort
  if strlen(s:error_message_on_issue_creation) > 0
    call s:NOTI.notify(s:error_message_on_issue_creation, 'WarningMsg')
  else
    call s:NOTI.notify('Issue creation success')
  endif
  call s:close_new_issue_win()
endfunction

function! s:close_new_issue_win() abort
  call s:close_this_buffer(s:new_issue_buffer)
endfunction
