"=============================================================================
" gitlib.vim --- gitlab plugin for spacevim
" Copyright (c) 
" Author: Necrosis Aumer < necrosis_t@mail.ru >
" URL: https://gitlab.com/necrosis/gitlab.vim
" License: GPLv3
"=============================================================================


let s:JOB = SpaceVim#api#import('job')
let s:BUFFER = SpaceVim#api#import('vim#buffer')
let s:NOTI = SpaceVim#api#import('notify')

let s:bufnr = -1
let s:cwd = ''
let s:taget_branch = ''
let s:source_branch = ''
let s:mr_create_flags = []

function! gitlab#mr#show_mr_list(flags) abort
  call s:show_mr_list(a:flags)
endfunction

function! gitlab#mr#new(...) abort
  let s:target_branch = get(a:000, 0, 'master')
  let s:source_branch = get(a:000, 1, '')
  let s:flags = a:000[2:]

  if empty(s:source_branch)
    call s:get_current_branch_name()
  else
    let s:cwd = expand('%:p:h')
    call s:create_new_mr_buffer()
  endif
endfunction

" -------------------------- MR View
function! s:open_mr_buffer() abort
  let bp = bufnr('%')
  edit gitlab://mr_list
  normal! "_dd
  setl nobuflisted
  setl nomodifiable
  setl nonumber norelativenumber
  setl buftype=nofile
  setl bufhidden=wipe
  setf gitlab-mr-list
  nnoremap <buffer><silent> <Cr> :call <SID>show_mr()<CR>
  nnoremap <buffer><silent> q :call <SID>close_win()<CR>
  return bufnr('%')
endfunction

function! s:on_mr_list_stdout(id, data, event) abort
  if !bufexists(s:bufnr)
    let s:bufnr = s:open_mr_buffer()
  endif
  call s:BUFFER.buf_set_lines(s:bufnr, getline('$') ==# '' ? 0 : -1 , -1, 0, a:data)
endfunction

function! s:on_mr_list_stderr(id, data, event) abort
  for data in a:data
    call s:NOTI.notify(data, 'WarningMsg')
  endfor
endfunction

function! s:show_mr_list(flags) abort
  let cmd = ['glab', 'mr', 'list'] + a:flags
  let s:cwd = expand('%:p:h')
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_mr_list_stderr'),
          \ 'on_stdout' : function('s:on_mr_list_stdout'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:close_mr_window() abort
  if bufexists(s:show_mr_buffer)
    exe 'bd ' . s:show_mr_buffer
  endif
endfunction

function! s:close_win() abort
  call s:close_mr_window()
  try
    bp
  catch /^Vim\%((\a\+)\)\=:E85/
    bd
  endtry
endfunction

let s:show_mr_buffer = -1
let s:mr_number = -1
function! s:show_mr() abort
  let s:mr_number = matchstr(getline('.'), '^\!\(\d\+\)')
  if s:mr_number == -1
    return
  endif
  if !bufexists(s:show_mr_buffer)
    let s:show_mr_buffer = s:open_show_mr_buffer()
  endif
  let cmd = ['glab', 'mr', 'show', s:mr_number[1:len(s:mr_number)]]
  let s:show_lines = []
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_show_mr_stderr'),
          \ 'on_stdout' : function('s:on_show_mr_stdout'),
          \ 'on_exit' : function('s:on_show_mr_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_show_mr_stdout(id, data, event) abort
  let s:show_lines += a:data
endfunction

function! s:on_show_mr_stderr(id, data, event) abort
  for data in a:data
    call s:NOTI.notify(data, 'WarningMsg')
  endfor
endfunction

function! s:on_show_mr_exit(id, data, event) abort
  call s:BUFFER.buf_set_lines(s:show_mr_buffer, 0 , -1, 0, s:show_lines)
endfunction

function! s:open_show_mr_buffer() abort
  rightbelow vsplit gitlab://show_mr
  normal! "_dd
  setl nobuflisted
  setl nomodifiable
  setl nonumber norelativenumber
  setl buftype=nofile
  setl bufhidden=wipe
  setf gitlab-mr
  nnoremap <buffer><silent> W :call <SID>show_mr_on_web()<CR>
  nnoremap <buffer><silent> e :call <SID>show_mr_diff()<CR>
  nnoremap <buffer><silent> a :call <SID>approve_mr()<CR>
  nnoremap <buffer><silent> u :call <SID>unapprove_mr()<CR>
  nnoremap <buffer><silent> m :call <SID>merge_mr()<CR>
  nnoremap <buffer><silent> c :call <SID>close_mr()<CR>
  nnoremap <buffer><silent> n :call <SID>create_new_comment_buffer()<CR>
  nnoremap <buffer><silent> q :q<CR>
  return bufnr('%')
endfunction

function! s:show_mr_on_web() abort
  let cmd = ['glab', 'mr', 'view', s:mr_number[1:len(s:mr_number)], '--web']
  call s:JOB.start(cmd,
        \ {
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

" ------- Approve
function! s:approve_mr() abort
  let cmd = ['glab', 'mr', 'approve', s:mr_number[1:len(s:mr_number)]]
  call s:JOB.start(cmd,
        \ {
          \ 'on_exit' : function('s:on_approve_mr_finished'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_approve_mr_finished(id, data, event) abort
  call s:NOTI.notify('MR approved by you')
endfunction

" ------- Unapprove
function! s:unapprove_mr() abort
  let cmd = ['glab', 'mr', 'revoke', s:mr_number[1:len(s:mr_number)]]
  call s:JOB.start(cmd,
        \ {
          \ 'on_exit' : function('s:on_unapprove_mr_finished'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_unapprove_mr_finished(id, data, event) abort
  let message = 'MR approve revoked'
  call s:NOTI.notify(message)
endfunction

" ------- Merge
function! s:merge_mr() abort
  let cmd = ['glab', 'mr', 'merge', s:mr_number[1:len(s:mr_number)], '-d']
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_merge_mr_error'),
          \ 'on_exit' : function('s:on_merge_mr_finished'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_merge_mr_error(id, data, event) abort
  for data in a:data
    call s:NOTI.notify(data, 'WarningMsg')
  endfor
endfunction

function! s:on_merge_mr_finished(id, data, event) abort
  call s:NOTI.notify('MR has been merged')
  exe ':q'
endfunction

" ------- Close
function! s:close_mr() abort
  let cmd = ['glab', 'mr', 'close', s:mr_number[1:len(s:mr_number)]]
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_close_mr_error'),
          \ 'on_exit' : function('s:on_close_mr_finished'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_close_mr_error(id, data, event) abort
  for data in a:data
    call s:NOTI.notify(data, 'WarningMsg')
  endfor
endfunction

function! s:on_close_mr_finished(id, data, event) abort
  call s:close_this_buffer(s:show_mr_buffer)
endfunction

" --------- Show Mr Diff

function! s:show_mr_diff() abort
  if s:mr_number == -1
    return
  endif
  if !bufexists(s:show_mr_buffer)
    let s:show_mr_buffer = s:open_show_mr_buffer()
  endif
  let cmd = ['glab', 'mr', 'diff', s:mr_number[1:len(s:mr_number)]]
  call s:BUFFER.buf_set_lines(s:show_mr_buffer, 0 , -1, 0, ['...loading'])
  let s:show_lines = []
  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_show_mr_stderr'),
          \ 'on_stdout' : function('s:on_show_mr_stdout'),
          \ 'on_exit' : function('s:on_show_mr_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

" -------------- MR NEW
let s:new_mr_buffer = -1
function! s:get_current_branch_name() abort
  let s:cwd = expand('%:p:h')
  let cmd = ['git', 'branch', '--show-current']
  call s:JOB.start(cmd,
        \ {
          \ 'on_stdout' : function('s:on_current_branch_name_stdout'),
          \ 'on_exit' : function('s:on_current_branch_name_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_current_branch_name_exit(id, data, event) abort
  call s:create_new_mr_buffer()
endfunction

function! s:on_current_branch_name_stdout(id, data, event) abort
  if len(get(a:data, 0, '')) > 0
    let s:source_branch = a:data[0]
  endif
endfunction

function! s:create_new_mr_buffer() abort
  if len(s:source_branch) == 0 || len(s:target_branch) == 0
    let message = 'Source branch is not set'
    call s:NOTI.notify(message, 'WarningMsg')
    return
  endif

  if !bufexists(s:new_mr_buffer)
    let s:new_mr_buffer = s:open_new_mr_buffer()
  endif

  let lines = 
        \ [
          \ '# ' . s:source_branch . ' -> ' . s:target_branch, 
          \ '# Write a title below', 
          \ '', 
          \ '# .. and description', 
          \ '', 
          \ 'assign: @me', 
        \ ]
  
  let reviewers = join(g:gitlab_reviewers_list, ', ')
  if strlen(reviewers) > 0
    call add(lines, "# reviewers: " . reviewers)
  else
    call add(lines, 'reviewers: ')
  endif

  call s:BUFFER.buf_set_lines(s:new_mr_buffer, 0, -1, 0, lines)
endfunction

function! s:open_new_mr_buffer() abort
  rightbelow vsplit gitlab://mr_create
  normal! "_dd
  setl nobuflisted
  setl modifiable
  setl nonumber norelativenumber
  exe 'file' fnameescape('_new_mr_temp_file_')
  setl buftype=acwrite
  setl bufhidden=wipe
  setf gitlab-mr-create
  nnoremap <buffer><silent> q :call <SID>close_new_mr_win()<CR>
  augroup NewMrCreation
    autocmd!
    autocmd BufWriteCmd _new_mr_temp_file_ call <SID>create_mr()
  augroup END
  return bufnr('%')
endfunction

function! s:create_mr() abort
  let durty_creation_info = s:BUFFER.buf_get_lines(s:new_mr_buffer, 0, -1, 0)
  let creation_info = []
  for line in durty_creation_info
    if matchstr(line, '^\s*\#') ==# ''
      call add(creation_info, line)
    endif
  endfor

  let title = get(creation_info, 0, '')
  call remove(creation_info, 0)

  let description_list = []
  let description_part = get(creation_info, 0, '')
  while matchstr(description_part, '^assign:') ==# '' && len(creation_info) > 0
    call add(description_list, description_part)
    call remove(creation_info, 0)
    let description_part = get(creation_info, 0, '')
  endwhile
  let description = join(description_list, '\n')

  let assing_part = substitute(get(creation_info, 0, ''), '^\s*assign:\s*', '', '')
  let reviewers = split(substitute(get(creation_info, 1, ''), '^\s*reviewers:\s*', '', ''), ',')

  let cmd = [
        \ 'glab', 'mr', 'new', 
        \ '-a', assing_part, 
        \ '-t', title, 
        \ '--description', description, 
        \ '-s', s:source_branch,
        \ '-b', s:target_branch,
        \ '--no-editor',
        \] + s:mr_create_flags

  let need_to_save = 0
  for reviewer in reviewers
    call add(cmd, '--reviewer')
    call add(cmd, reviewer)

    if index(g:gitlab_reviewers_list, reviewer) < 0
      call add(g:gitlab_reviewers_list, reviewer)
      let need_to_save = 1
    endif
  endfor

  if need_to_save == 1
    call gitlab#review#save()
  endif

  setl nomodified

  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_create_mr_stderr'),
          \ 'on_exit' : function('s:on_create_mr_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

let s:error_message_on_creation = ''
function! s:on_create_mr_stderr(id, data, event) abort
  for data in a:data
    let s:error_message_on_creation += data
  endfor
endfunction

function! s:on_create_mr_exit(id, data, event) abort
  if strlen(s:error_message_on_creation) > 0
    call s:NOTI.notify(s:error_message_on_creation, 'WarningMsg')
  else
    call s:NOTI.notify('MR creation success')
  endif
  call s:close_new_mr_win()
endfunction

function! s:close_new_mr_win() abort
  call s:close_this_buffer(s:new_mr_buffer)
endfunction

function! s:close_this_buffer(buffer) abort
  if bufexists(a:buffer)
    exe 'bd! ' . a:buffer
  endif

  try
    bp
  catch /^Vim\%((\a\+)\)\=:E85/
    bd
  endtry
endfunction

" -------------------------- MR Comment
let s:new_comment_buffer = -1
function! s:create_new_comment_buffer() abort
  if !bufexists(s:new_comment_buffer)
    let s:new_comment_buffer = s:open_new_comment_buffer()
  endif

  call s:BUFFER.buf_set_lines(s:new_comment_buffer, 0, -1, 0, ["# Write comment below"])
endfunction

function! s:open_new_comment_buffer() abort
  rightbelow split gitlab://mr_comment
  normal! "_dd
  setl nobuflisted
  setl modifiable
  setl nonumber norelativenumber
  exe 'file' fnameescape('_new_mr_comment_temp_file_')
  setl buftype=acwrite
  setl bufhidden=wipe
  setf gitlab-comment
  nnoremap <buffer><silent> q :call <SID>close_new_comment_win()<CR>
  augroup NewMRCommentCreation
    autocmd!
    autocmd BufWriteCmd _new_mr_comment_temp_file_ call <SID>push_comment()
  augroup END
  return bufnr('%')
endfunction

function! s:push_comment() abort
  let durty_comment = s:BUFFER.buf_get_lines(s:new_comment_buffer, 1, -1, 0)

  let durty_comment_split = split(join(durty_comment, '\n'), '"')
  let covered_comment = join(durty_comment_split, '\\"')

  let cmd = [
        \ 'glab', 'mr', 'note',
        \ '--message', covered_comment,
        \ s:mr_number[1:len(s:mr_number)]
        \]

  call s:JOB.start(cmd,
        \ {
          \ 'on_stderr' : function('s:on_comment_mr_error'),
          \ 'on_exit' : function('s:on_comment_mr_finished'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

let s:error_message_on_creation = ''
function! s:on_comment_mr_error(id, data, event) abort
  for data in a:data
    let s:error_message_on_creation += data
  endfor
endfunction

function! s:on_comment_mr_finished(id, data, event) abort
  if strlen(s:error_message_on_creation) > 0
    call s:NOTI.notify(s:error_message_on_creation, 'WarningMsg')
  else
    call s:NOTI.notify('Comment created')
  endif
  call s:close_new_comment_win()
endfunction

function! s:close_new_comment_win() abort
  call s:close_this_buffer(s:new_comment_buffer)
endfunction
