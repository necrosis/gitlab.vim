"=============================================================================
" gitlib.vim --- gitlab plugin for spacevim
" Copyright (c) 
" Author: Necrosis Aumer < necrosis_t@mail.ru >
" URL: https://gitlab.com/necrosis/gitlab.vim
" License: GPLv3
"=============================================================================

let s:JOB = SpaceVim#api#import('job')
let s:NOTI = SpaceVim#api#import('notify')


let s:gitlab_cache_file = ''
function! s:setup() abort
  let current_project_path = expand('%:p:h')
  let s:gitlab_cache_file = current_project_path . '/.SpaceVim.d/gitlab.reviewers'
endfunction

function! gitlab#utils#startup_setup() abort
  call gitlab#utils#load_reviewers()
  call gitlab#utils#load_branches()
endfunction

function! gitlab#utils#load_reviewers() abort
  call s:setup()
  if filereadable(s:gitlab_cache_file)
    let g:gitlab_reviewers_list = readfile(s:gitlab_cache_file)
  elseif !exists('g:gitlab_reviewers_list')
    let g:gitlab_reviewers_list = []
  endif
endfunction

function! gitlab#utils#save_reviewers() abort
  call s:setup()
  call writefile(g:gitlab_reviewers_list, s:gitlab_cache_file)
endfunction

let s:git_list_output = []
function! gitlab#utils#load_branches() abort
  let s:cwd = expand('%:p:h')
  let cmd = ['git', 'branch', '--list']
  call s:JOB.start(cmd,
        \ {
          \ 'on_stdout' : function('s:on_git_list_stdout'),
          \ 'on_exit' : function('s:on_git_list_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_git_list_stdout(id, data, event) abort
  for data in a:data
    let trimmed_data = trim(data, '* ')
    if index(s:git_list_output, trimmed_data) == -1
      call add(s:git_list_output, trimmed_data)
    endif
  endfor
endfunction

function! s:on_git_list_exit(id, data, event) abort
  let g:gitlab_existing_branches = s:git_list_output
endfunction

"------------------------ default run
let s:run_command_output = []
function! gitlab#utils#run(command) abort
  let s:cwd = expand('%:p:h')
  call s:JOB.start(a:command,
        \ {
          \ 'on_stdout' : function('s:on_run_std'),
          \ 'on_stderr' : function('s:on_run_std'),
          \ 'on_exit' : function('s:on_run_exit'),
          \ 'cwd': s:cwd,
          \ }
          \ )
endfunction

function! s:on_run_std(id, data, event) abort
  let s:run_command_output += a:data
endfunction

function! s:on_run_exit(id, data, event) abort
  call s:NOTI.notify(join(s:run_command_output, "\n"), 'WarningMsg')
endfunction
